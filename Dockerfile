FROM nginx:latest

RUN apt-get update && apt-get install -y vim net-tools && rm -rf /var/lib/apt/lists/*

COPY index.html /usr/share/nginx/html/index.html

EXPOSE 80